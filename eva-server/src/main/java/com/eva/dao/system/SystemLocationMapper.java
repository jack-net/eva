package com.eva.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eva.dao.system.model.SystemLocation;

/**
 * @author Eva.Caesar Liu
 * @date 2021/08/06 22:58
 */
public interface SystemLocationMapper extends BaseMapper<SystemLocation> {

}
