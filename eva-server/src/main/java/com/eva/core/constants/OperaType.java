package com.eva.core.constants;

/**
 * 操作类型
 * @author Eva.Caesar Liu
 * @date 2021/08/06 22:58
 */
public interface OperaType {

    /**
     * 创建
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    interface Create {}

    /**
     * 修改
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    interface Update {}

    /**
     * 修改状态
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    interface UpdateStatus {}
}
