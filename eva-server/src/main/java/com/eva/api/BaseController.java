package com.eva.api;

import com.eva.core.model.LoginUserInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;

/**
 * Controller基类
 * @author Eva.Caesar Liu
 * @date 2021/08/06 22:58
 */
@Slf4j
public class BaseController {

    /**
     * 获取当前登录用户
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    protected LoginUserInfo getLoginUser () {
        return (LoginUserInfo)SecurityUtils.getSubject().getPrincipal();
    }

}
