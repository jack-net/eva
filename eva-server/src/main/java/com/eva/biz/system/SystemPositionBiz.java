package com.eva.biz.system;


import com.eva.dao.system.model.SystemPosition;
import com.eva.dao.system.vo.SystemPositionListVO;

import java.util.List;

public interface SystemPositionBiz {

    /**
     * 新建
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    Integer create(SystemPosition systemPosition);

    /**
     * 编辑
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    void updateById(SystemPosition systemPosition);

    /**
     * 查询岗位列表树
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    List<SystemPositionListVO> findTree();

    /**
     * 删除
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    void deleteById(Integer id);

    /**
     * 批量删除
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    void deleteByIdInBatch(List<Integer> ids);
}
